/**
  This testing framework has been developed/overhauled over time, primarily by:
  Chris Czyzewicz
  Ben Sunshine-Hill
  Cory Boatright 
  
  While the three were PhD students at UPenn (through 2013).  This particular version has some
  modifications by Cory since joining the faculty of Grove City College.
  
  Last Edited by Samuel Kenney, Grove CIty College: 4/28/2017
*/

#ifndef INTERSECTION_H
#define INTERSECTION_H_H

#include "gMatrix4.h"
#include "gVector4.h"
#include <math.h>
#include <limits>
#include <algorithm>

// IMPORTANT NOTE:
// Although these functions are useful for testing, they're not good to use for the
// actual raycasting. In particular, using these functions means you have to invert
// an object's transformation matrix for every ray test you run against it.
// It's much better to cache an object's inverted transformation matrix in the node,
// and then use that to transform the ray, rather than invert it in your raycast.
// Therefore, make these functions wrappers around your actual intersection functions,
// and use those functions in your raytracer.


class intersection
{
public:
	intersection();
	intersection(gMatrix4,gMatrix4,gVector4,gVector4,gVector4);

	~intersection();

	void Set_Up_Intersect(const gVector4& P0, const gMatrix4& T);

	// Tries to find the intersection of a ray and a sphere.
	// P0 is the position from which the ray emanates; V0 is the
	// direction of the ray.
	// matrix is the transformation matrix of the sphere.
	// This function should return the smallest positive t-value of the intersection
	// (a point such that P0+t*V0 intersects the sphere), or -1 if there is no
	// intersection.
	//tests if intersects with sphere
	double test_ray_sphere_intersection(const gVector4&);

	// Tries to find the intersection of a ray and a triangle.
	// This is just like the above function, but it intersects the ray with a
	// triangle instead. The parameters p1, p2, and p3 specify the three
	// points of the triangle, in object space.
	// tests if intersects with triangle
	double test_ray_poly_intersection(const gVector4&,  const gVector4& , const gVector4& , const gVector4&);

	// This is just like RaySphereIntersect, but with a unit cube instead of a
	// sphere. A unit cube extends from -0.5 to 0.5 in all axes.
	// tests if intersects with cube
	double test_ray_cube_intersection(const gVector4&);

	//epsilon error checking
	bool approxEquals(float a, float b)
	{
		const float epsilon = 0.000001f;
		if (a == b) {
			return true;
		}
		else if(a * b == 0.0f) {
			return fabs(a - b) < epsilon * epsilon;
		}
		else {
			return fabs(a - b) / (fabs(a) + fabs(b)) < epsilon;
		}
	}

	void setC(gMatrix4 mat){this->C = mat;}
	gMatrix4 getC(void){return this->C;}

	void setobjectP0(gVector4 P0){this->objectP0 = P0;}
	gVector4 getobjectP0(void){return this->objectP0;}

	void setobjectV0(gVector4 V0){this->objectV0 = V0;}
	gVector4 getobjectV0(void){return this->objectV0;}

private:
	gMatrix4 C;
	gMatrix4 invertMatrix;
	gMatrix4 invert_starMatrix;
	gVector4 center;
	gVector4 objectP0;
	gVector4 objectV0;

};


#endif