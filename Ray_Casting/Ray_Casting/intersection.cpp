/**
  This testing framework has been developed/overhauled over time, primarily by:
  Chris Czyzewicz
  Ben Sunshine-Hill
  Cory Boatright 
  
  While the three were PhD students at UPenn (through 2013).  This particular version has some
  modifications by Cory since joining the faculty of Grove City College.
  
  Last revised 4/10/2017

  Last Edited by Samuel Kenney, Grove CIty College: 4/28/2017
*/

#include "intersection.h"

//constructors
intersection::intersection(){}

intersection::intersection(gMatrix4 invertMatrix,gMatrix4 invert_starMatrix,gVector4 center,gVector4 objectP0,gVector4 objectV0)
	:invertMatrix(invertMatrix), invert_starMatrix(invert_starMatrix), center(center), objectP0(objectP0), objectV0(objectV0)
{}

//takes a point and matrix--- no longer takes the ray since that needs to be calculated each time
void intersection::Set_Up_Intersect(const gVector4& P0, const gMatrix4& T) {
	setC(T);
	//get and set center
	gVector4 center(0.0f,0.0f,0.0f,1.0f);
	this->center = center;

	//invert matrix
	invertMatrix = T.invert(T);
	//invert matrix star
	invert_starMatrix = T.invertstar(T);
	//transform matrix to object space
	objectP0 = invertMatrix * P0;
}

//arg needs to be the ray at the pixel
double intersection::test_ray_sphere_intersection(const gVector4& V0){
	//transofrm vector to object space
	objectV0 = invert_starMatrix * V0;

	//calculate a
	float a = objectV0 * objectV0;
	//calculate b
	float b = (2.0f * objectV0) * (objectP0 - center);
	//get length of objectP0 - center
	float objectP0_Length = (objectP0 - center).length();
	//calculate c
	float c = ((objectP0_Length) * (objectP0_Length)) - 1.0f;

	//calculate value under sqrt to make sure it is not negative
	float under_sqrt = ((b*b) - 4 * a * c);

	//make sure not to take sqrt of a neg number
	if ((under_sqrt > 0.0f) || (approxEquals(under_sqrt, 0.0f))){
		float firstT = (-b + sqrt((b*b) - 4 * a * c)) / (2 * a);
		float secondT = (-b - sqrt((b*b) - 4 * a * c)) / (2 * a);

		//if both are negative return -1.0f
		if ((firstT < 0.0f) && (secondT < 0.0f)){
			return -1.0;
		}else {
			//return the smallest of the two
			if (firstT > secondT)
				return secondT;
			else
				return firstT;
		}
	}

	return -1;
}

// Tries to find the intersection of a ray and a triangle.
// This is just like the above function, but it intersects the ray with a
// triangle instead. The parameters p1, p2, and p3 specify the three
// points of the triangle, in object space.

// tests if intersects
double intersection::test_ray_poly_intersection(const gVector4& V0, const gVector4& p1, const gVector4& p2, const gVector4& p3){
	//transofrm V0 to object space
	objectV0 = invert_starMatrix * V0;

	//find the area for each of the parts in the triangle
	float S = gMatrix4::identity().area_from_three_points(p1, p2, p3);
	if (C.approxEquals(S, 0.0f))
		return -1.0f;

	//get two vectors from the triangle to get the direction of the normal by crossing them
	gVector4 first = p1-p3;
	gVector4 second = p2-p3;
	gVector4 N = first % second;

	//normalize N
	N.normalize();

	//if denom is 0.0f;
	if (C.approxEquals((N * objectV0), 0.0f)){return -1.0;}
	//solve for f
	float t = (N * (p1 - objectP0))/(N * objectV0);
	if (C.approxEquals(t, -1.0f)){return -1.0f;}

	//find P
	gVector4 P = objectP0 + (t * objectV0);

	//find area for the three triangles within S
	float S1 = (gMatrix4::identity().area_from_three_points(P, p2, p3)) / S;
	float S2 = (gMatrix4::identity().area_from_three_points(P, p3, p1)) / S;
	float S3 = (gMatrix4::identity().area_from_three_points(P, p1, p2)) / S;

	float total_area = S1 + S2 + S3;
	if (total_area < 0.0f)
		return -1.0;

	//if total area is 1.0f and the values of S1, S2, S3 are less than 1.0f and are with the range of 0 <= x <= 1.0f, return f
	if (( C.approxEquals(total_area, 1.0f)) && (S1 && S2 && S3 < 1.0f) && 
		(S1 && S2 && S3 > 0.0f) || 
		(C.approxEquals(total_area, 1.0f)) 
		|| (C.approxEquals(total_area, 0.0f))){
			return t;
	}

	return -1;
}

// This is just like RaySphereIntersect, but with a unit cube instead of a
// sphere. A unit cube extends from -0.5 to 0.5 in all axes.

// tests if intersects with cube
double intersection::test_ray_cube_intersection(const gVector4& V0){
	//transofrm V0 to object space
	objectV0 = invert_starMatrix * V0;

	//set values for tnear and tfar
	float tnear = -std::numeric_limits<float>::infinity();
	float tfar = std::numeric_limits<float>::infinity();

	//check if x is perpendicular
	if (C.approxEquals(0.0f, objectV0[0])){
		//make sure it is within the bound
		if ((objectP0[0] > 0.5f) || (objectP0[0] < -0.5f)){
			return -1;
		}
	} else {
		//solve for x_tnear and x_tfar
		float x_tnear = (-0.5f - objectP0[0])/objectV0[0];
		float x_tfar = (0.5f - objectP0[0])/objectV0[0];

		//if ray is behind, switch values for near and far to reflect that
		if (x_tfar < x_tnear) {std::swap<float>(x_tfar, x_tnear);}

		//update global tnear and tfar with respective max and min values
		if (x_tnear > tnear){tnear = x_tnear;}
		if (x_tfar < tfar){tfar = x_tfar;}
	}

	if (C.approxEquals(0.0f, objectV0[1])){
		//make sure it is within the bound
		if ((objectP0[1] > 0.5f) || (objectP0[1] < -0.5f)){
			return -1;
		}
	} else {
		//solve for y_tnear and y_tfar
		float y_tnear = (-0.5f - objectP0[1])/objectV0[1];
		float y_tfar = (0.5f - objectP0[1])/objectV0[1];

		//if ray is behind, switch values for near and far to reflect that
		if (y_tfar < y_tnear) {std::swap<float>(y_tfar, y_tnear);}

		//update global tnear and tfar with respective max and min values
		if (y_tnear > tnear){tnear = y_tnear;}
		if (y_tfar < tfar){tfar = y_tfar;}
	}

	if (C.approxEquals(0.0f, objectV0[2])){
		//make sure it is within the bound
		if ((objectP0[2] > 0.5f) || (objectP0[2] < -0.5f)){
			return -1;
		}
	} else {
		//solve for z_tnear and z_tfar
		float z_tnear = (-0.5f - objectP0[2])/objectV0[2];
		float z_tfar = (0.5f - objectP0[2])/objectV0[2];

		//if ray is behind, switch values for near and far to reflect that
		if (z_tfar < z_tnear) {std::swap<float>(z_tfar, z_tnear);}

		//update global tnear and tfar with respective max and min values
		if (z_tnear > tnear){tnear = z_tnear;}
		if (z_tfar < tfar){tfar = z_tfar;}
	}

	//if the far is less than the near, return -1.0f
	if (tfar < tnear) 
		return -1.0f;
	//else return closest intersection point
	else 
		return tnear;

	return -1;
}