/**********************************************
* Author: Samuel Kenney 4/25/2017
*
* Grove City College: Introduction to Graphics
*
* Dr. Boatright
*
* Implements driver for testing Ray Castinng
*
**********************************************/

#include "EasyBMP.h"
#include "camera.h"
#include "intersection.h"
/*
EYEP: 0 0 10 (position of the camera)
AT: 0 0 0 (always looking at the origin)
VDIR: 0 0 -1
UVEC: 0 1 0

LPOS: 5 5 5
LCOL 1 1 1

BRGB: 0.0 0.0 0.0
MRGB: 0.294 0.0 0.51

Width: 640
Height: 480

VDIR: 0 0 -1
UVEC: 0 1 0
FOVY: 45 degrees

*/

int main (int argv, char** argc){
	//the matrix used and the camera position are subject to change from the use of these options
	//set identity matrix
	gMatrix4 identity = gMatrix4::identity();
	//set up three matrices for other images
	gMatrix4 transY = gMatrix4::translate3D(0.0f, 1.0f, 0.0);
	gMatrix4 rot45Z = gMatrix4::rotateZ(45.0f);
	gMatrix4 scale15X = gMatrix4::scale3D(1.5f,1.0f,1.0f);
	gMatrix4 transObject =  transY  * rot45Z *scale15X ;

	//camPos: (position of the camera)
	//each of the three options
	gVector4 camPos0(0.0f, 0.0f, 3.0f, 1.0f);
	gVector4 camPos1(-3.0f, 0.0f, 0.0f, 1.0f);
	gVector4 camPos2(1.0f, 1.0f, 1.0f, 1.0f);

	std::cout << std::endl;

	std::cout << "***** Starting generation of Sphere *****" << std::endl;

	//generate all 6 of sphere images
	//generate all three images 
	for (int i = 0; i < 6; i++)
	{
		gVector4 camSphere;
		gMatrix4 transSphere;
		char * output_bmp_sphere;

		switch(i)
		{
		case 0:
			std::cout << "First"<< std::endl;
			camSphere = camPos0;
			transSphere = identity;
			output_bmp_sphere = "test_output_sphere1.bmp";
			break;
		case 1:
			std::cout << "Second"<< std::endl;
			camSphere= camPos1;
			transSphere = identity;
			output_bmp_sphere = "test_output_sphere2.bmp";
			break;
		case 2:
			std::cout << "Third"<< std::endl;
			camSphere= camPos2;
			transSphere = identity;
			output_bmp_sphere = "test_output_sphere3.bmp";
			break;
		case 3:
			std::cout << "Fourth"<< std::endl;
			camSphere= camPos0;
			transSphere = transObject;
			output_bmp_sphere = "test_output_sphere_trans1.bmp";
			break;
		case 4:
			std::cout << "Fifth"<< std::endl;
			camSphere= camPos1;
			transSphere = transObject;
			output_bmp_sphere = "test_output_sphere_trans2.bmp";
			break;
		case 5:
			std::cout << "Sixth"<< std::endl;
			camSphere= camPos2;
			transSphere = transObject;
			output_bmp_sphere = "test_output_sphere_trans3.bmp";
			break;
		}

		std::cout << "***** Generating Camera *****" << std::endl;
		//set values for setting up the camera
		unsigned int width = 1280, height = 960;

		//AT: 0 0 0 (always looking at the origin)
		gVector4 at(0.0f, 0.0f, 0.0f, 1.0f);
		//VDIR: 0 0 -1
		gVector4 vdir = at - camSphere;
		//UVEC: 0 1 0
		gVector4 uvec(0.0f, 1.0f, 0.0f, 1.0f);
		//FOVY: 45 degrees
		float fovy = 45.0f;
		//LPOS
		gVector4 lpos(5.0f,5.0f,5.0f, 1.0f);                                                                        
		//LCOL
		gVector4 lcol(1.0f, 1.0f, 1.0f, 1.0f);
		//BRGB
		gVector4 BRGB(0.0f, 0.0f, 0.0f, 0.0f);
		//MRGB
		gVector4 MRGB(0.3f, 0.5f, 0.8f, 0.0f);

		//instantiate camera
		camera ray_sphere(width, height, camSphere, vdir, uvec, fovy);

		//generate vectors
		ray_sphere.FirstBasisVector();
		ray_sphere.SecondBasisVector();
		ray_sphere.ThirdBasisVector();
		ray_sphere.calculateMVH();

		std::cout << "***** Intersection: Sphere*****" << std::endl;
		//set up for sphere intersection
		intersection* sphereIntersect = new intersection();
		sphereIntersect->Set_Up_Intersect(camSphere, transSphere);

		std::cout << "***** Testing Sphere *****" << std::endl;
		BMP output1;
		output1.SetSize(width, height);
		output1.SetBitDepth(24);

		//set pixel color to be black
		gVector4 pixelCol(0.0f, 0.0f, 0.0f, 0.0f);
		//calculate the color for each of the pixels
		for(unsigned int x = 0; x < width;++x ) {
			for(unsigned int y = 0; y < height; ++y) {
				//generate ray
				gVector4 ray = ray_sphere.generateRay(x, y);
				//test whether or not it intersects
				//t wil be the same for local and world space
				double t = sphereIntersect->test_ray_sphere_intersection(ray);
				if (t == -1.0f){
					pixelCol = BRGB;
				} else{
					//calculate intersection point
					gVector4 intersect(0.0f, 0.0f, 0.0f, 0.0f); 
					//R = P + vt
					//calculates the intersection point in local space
					intersect = sphereIntersect->getobjectP0() + sphereIntersect->getobjectV0() * (float)t;

					//add normal
					gVector4 normal = intersect;

					//transform intersection point to world space
					intersect = sphereIntersect->getC()* intersect;

					//transform nprmal back into world space
					normal = sphereIntersect->getC()* normal;
					normal.normalize();

					//use Lambertian shading
					//hardcode ambient light
					gVector4 ambient(0.1f,0.1f,0.1f,0.0f);

					//in world space
					gVector4 light = lpos - intersect;
					light.normalize();
					gVector4 diffuse = (normal * light) * MRGB;
					//do not allot an values less than zero
					for (int i = 0; i < 4; i++)
					{
						if (diffuse[i] < 0.0f){
							diffuse[i] = 0.0f;
						}
					}
					//set pixel color values
					pixelCol[0] = ambient[0] + diffuse[0];
					pixelCol[1] = ambient[1] + diffuse[1];
					pixelCol[2] = ambient[2] + diffuse[2];

				}

				if (pixelCol[0] >=1){
					output1(x, y)->Red = 255;
				} else {
					output1(x, y)->Red = std::abs(pixelCol[0])*255;
				}
				if (pixelCol[2] >=1){
					output1(x,y)->Blue = 255;
				} else {
					output1(x, y)->Blue = std::abs(pixelCol[2])*255;
				}
				if (pixelCol[1] >=1){
					output1(x, y)->Green = 255;
				} else {
					output1(x, y)->Green = std::abs(pixelCol[1])*255;
				}
			}
		}
		output1.WriteToFile(output_bmp_sphere);
		std::cout << "*********************************************" << std::endl;
		std::cout << std::endl; 
	}


	std::cout << "***** Starting generation of Triangles *****" << std::endl;

	//generate all 6 of triangle images
	//generate all three images 
	for (int i = 0; i < 6; i++)
	{
		gVector4 camTriangle;
		gMatrix4 transTriangle;
		char * output_bmp_triangle;

		switch(i)
		{
		case 0:
			std::cout << "First"<< std::endl;
			camTriangle= camPos0;
			transTriangle = identity;
			output_bmp_triangle = "test_output_triangle1.bmp";
			break;
		case 1:
			std::cout << "Second"<< std::endl;
			camTriangle= camPos1;
			transTriangle = identity;
			output_bmp_triangle = "test_output_triangle2.bmp";
			break;
		case 2:
			std::cout << "Third"<< std::endl;
			camTriangle= camPos2;
			transTriangle = identity;
			output_bmp_triangle = "test_output_triangle3.bmp";
			break;
		case 3:
			std::cout << "Fourth"<< std::endl;
			camTriangle= camPos0;
			transTriangle = transObject;
			output_bmp_triangle = "test_output_triangle_trans1.bmp";
			break;
		case 4:
			std::cout << "Fifth"<< std::endl;
			camTriangle= camPos1;
			transTriangle = transObject;
			output_bmp_triangle = "test_output_triangle_trans2.bmp";
			break;
		case 5:
			std::cout << "Sixth"<< std::endl;
			camTriangle= camPos2;
			transTriangle = transObject;
			output_bmp_triangle = "test_output_triangle_trans3.bmp";
			break;
		}

		std::cout << "***** Generating Camera *****" << std::endl;
		//set values for setting up the camera
		unsigned int width = 1280, height = 960;

		//AT: 0 0 0 (always looking at the origin)
		gVector4 at(0.0f, 0.0f, 0.0f, 1.0f);
		//VDIR: 0 0 -1
		gVector4 vdir = at - camTriangle;
		//UVEC: 0 1 0
		gVector4 uvec(0.0f, 1.0f, 0.0f, 1.0f);
		//FOVY: 45 degrees
		float fovy = 45.0f;
		//LPOS
		gVector4 lpos(5.0f,5.0f,5.0f, 1.0f);                                                                        
		//LCOL
		gVector4 lcol(1.0f, 1.0f, 1.0f, 1.0f);
		//BRGB
		gVector4 BRGB(0.0f, 0.0f, 0.0f, 0.0f);
		//MRGB
		gVector4 MRGB(0.8f, 0.1f, 0.7f, 0.0f);

		//instantiate camera
		camera ray_triangle(width, height, camTriangle, vdir, uvec, fovy);

		//generate vectors
		ray_triangle.FirstBasisVector();
		ray_triangle.SecondBasisVector();
		ray_triangle.ThirdBasisVector();
		ray_triangle.calculateMVH();
		std::cout << "***** Intersection: Triangles*****" << std::endl;

		//set up for sphere intersection
		intersection* triangleIntersect = new intersection();
		triangleIntersect->Set_Up_Intersect(camTriangle, transTriangle);

		gVector4 p1(0.0f, 1.0f, 0.0f, 1.0f), p2(1.0f, 0.0f, 0.0f, 1.0f), p3(-1.0f, 0.0f, 0.0f, 1.0f);

		//get two vectors from the triangle to get the direction of the normal by crossing them
		gVector4 first = p1-p3;
		gVector4 second = p2-p3;
		gVector4 N = second % first;

		//normalize N
		N.normalize();

		std::cout << "***** Testing Triangle *****" << std::endl;
		BMP output2;
		output2.SetSize(width, height);
		output2.SetBitDepth(24);

		//set pixel color to be black
		gVector4 pixelCol(0.0f, 0.0f, 0.0f, 0.0f);
		//calculate the color for each of the pixels
		for(unsigned int x = 0; x < width;++x ) {
			for(unsigned int y = 0; y < height; ++y) {
				//generate ray
				gVector4 ray = ray_triangle.generateRay(x, y);
				//test whether or not it intersects
				double t = triangleIntersect->test_ray_poly_intersection(ray,p1,p2,p3);
				if (t == -1.0f){
					pixelCol = BRGB;
				} else{
					//calculate intersection point
					gVector4 intersect(0.0f, 0.0f, 0.0f, 1.0f); 
					//R = P + vt
					//calculates the intersection point in local space
					intersect = triangleIntersect->getobjectP0() + triangleIntersect->getobjectV0() * (float)t;
					//transform intersection to world space
					intersect = triangleIntersect->getC() * intersect;

					//use Lambertian shading
					//hardcode ambient light
					gVector4 ambient = BRGB * 0.3f;

					//in world space
					gVector4 light = lpos - intersect;
					light.normalize();

					//normal calculated before
					gVector4 diffuse = (N * light) * MRGB;
					//do not allot an values less than zero
					for (int i = 0; i < 4; i++)
					{
						if (diffuse[i] < 0.0f){
							diffuse[i] = 0.0f;
						}
					}
					//set pixel color values
					pixelCol[0] = ambient[0] + diffuse[0];
					pixelCol[1] = ambient[1] + diffuse[1];
					pixelCol[2] = ambient[2] + diffuse[2];

				}

				if (pixelCol[0] >=1){
					output2(x, y)->Red = 255;
				} else {
					output2(x, y)->Red = std::abs(pixelCol[0])*255;
				}
				if (pixelCol[2] >=1){
					output2(x,y)->Blue = 255;
				} else {
					output2(x, y)->Blue = std::abs(pixelCol[2])*255;
				}
				if (pixelCol[1] >=1){
					output2(x, y)->Green = 255;
				} else {
					output2(x, y)->Green = std::abs(pixelCol[1])*255;
				}
			}
		}
		output2.WriteToFile(output_bmp_triangle);
		std::cout << "*********************************************" << std::endl;
		std::cout << std::endl;
		std::cout << "*********************************************" << std::endl;
		std::cout << std::endl;

	}


	std::cout << "***** Starting generation of Cubes *****" << std::endl;
	gVector4 camCube;
	gMatrix4 transCube;
	char * output_bmp_cube;

	for (int i = 0; i < 6; i++)
	{
		switch(i)
		{
		case 0:
			std::cout << "First"<< std::endl;
			camCube= camPos0;
			transCube = identity;
			output_bmp_cube = "test_output_cube1.bmp";
			break;
		case 1:
			std::cout << "Second"<< std::endl;
			camCube= camPos1;
			transCube = identity;
			output_bmp_cube = "test_output_cube2.bmp";
			break;
		case 2:
			std::cout << "Third"<< std::endl;
			camCube= camPos2;
			transCube = identity;
			output_bmp_cube = "test_output_cube3.bmp";
			break;
		case 3:
			std::cout << "Fourth"<< std::endl;
			camCube= camPos0;
			transCube = transObject;
			output_bmp_cube = "test_output_cube_trans1.bmp";
			break;
		case 4:
			std::cout << "Fifth"<< std::endl;
			camCube= camPos1;
			transCube = transObject;
			output_bmp_cube = "test_output_cube_trans2.bmp";
			break;
		case 5:
			std::cout << "Sixth"<< std::endl;
			camCube= camPos2;
			transCube = transObject;
			output_bmp_cube = "test_output_cube_trans3.bmp";
			break;
		}

		std::cout << "***** Generating Camera *****" << std::endl;
		//set values for setting up the camera
		unsigned int width = 1280, height = 960;

		//AT: 0 0 0 (always looking at the origin)
		gVector4 at(0.0f, 0.0f, 0.0f, 1.0f);
		//VDIR: 0 0 -1
		gVector4 vdir = at - camCube;
		//UVEC: 0 1 0
		gVector4 uvec(0.0f, 1.0f, 0.0f, 1.0f);
		//FOVY: 45 degrees
		float fovy = 45.0f;
		//LPOS
		gVector4 lpos(5.0f,5.0f,5.0f, 1.0f);                                                                        
		//LCOL
		gVector4 lcol(1.0f, 1.0f, 1.0f, 1.0f);
		//BRGB
		gVector4 BRGB(0.0f, 0.0f, 0.0f, 0.0f);
		//MRGB
		gVector4 MRGB(0.2f, 0.5f, 0.4f, 0.0f);

		//instantiate camera
		camera ray_cube(width, height, camCube, vdir, uvec, fovy);

		//generate vectors
		ray_cube.FirstBasisVector();
		ray_cube.SecondBasisVector();
		ray_cube.ThirdBasisVector();
		ray_cube.calculateMVH();

		std::cout << "***** Intersection: Cube*****" << std::endl;
		//set up for cube intersection
		intersection* cubeIntersect = new intersection();
		cubeIntersect->Set_Up_Intersect(camCube, transCube);

		std::cout << "***** Testing Cube *****" << std::endl;
		BMP output1;
		output1.SetSize(width, height);
		output1.SetBitDepth(24);

		//set pixel color to be black
		gVector4 pixelCol(0.0f, 0.0f, 0.0f, 0.0f);
		//calculate the color for each of the pixels
		for(unsigned int x = 0; x < width;++x ) {
			for(unsigned int y = 0; y < height; ++y) {
				//generate ray
				gVector4 ray = ray_cube.generateRay(x, y);
				//test whether or not it intersects
				double t = cubeIntersect->test_ray_cube_intersection(ray);
				if (t == -1.0f){
					pixelCol = BRGB;
				} else{
					//calculate intersection point
					gVector4 intersect(0.0f, 0.0f, 0.0f, 1.0f); 
					gVector4 normalCube(0.0f, 0.0f, 0.0f, 0.0f);
					//R = P + vt
					//calculates the intersection point in local space
					intersect = cubeIntersect->getobjectP0() + cubeIntersect->getobjectV0() * (float)t;


					gVector4 N(0.0f, 0.0f, 0.0f, 0.0f);
					if (cubeIntersect->approxEquals(intersect[0], -0.5f)){
						normalCube[0] = -1.0f;
					}
					if (cubeIntersect->approxEquals(intersect[0], 0.5f)){
						normalCube[0] = 1.0f;
					}
					if (cubeIntersect->approxEquals(intersect[1], -0.5f)){
						normalCube[1] = -1.0f;
					}
					if (cubeIntersect->approxEquals(intersect[1], 0.5f)){
						normalCube[1] = 1.0f;
					}
					if (cubeIntersect->approxEquals(intersect[2], -0.5f)){
						normalCube[2] = -1.0f;
					}
					if (cubeIntersect->approxEquals(intersect[2], 0.5f)){
						normalCube[2] = 1.0f;
					}

					//use Lambertian shading
					//hardcode ambient light
					gVector4 ambient(0.1f,0.1f,0.1f,0.0f);


					normalCube = cubeIntersect->getC() * normalCube;
					//transform normal back into world space
					normalCube.normalize();

					//transform intersection to world space
					intersect = cubeIntersect->getC() * intersect;

					gVector4 light = lpos - intersect;
					light.normalize();
					gVector4 diffuse = (normalCube * light) * MRGB;
					//do not allot an values less than zero
					for (int i = 0; i < 4; i++)
					{
						if (diffuse[i] < 0.0f){
							diffuse[i] = 0.0f;
						}
					}
					//set pixel color values
					pixelCol[0] = ambient[0] + diffuse[0];
					pixelCol[1] = ambient[1] + diffuse[1];
					pixelCol[2] = ambient[2] + diffuse[2];

				}

				if (pixelCol[0] >=1){
					output1(x, y)->Red = 255;
				} else {
					output1(x, y)->Red = std::abs(pixelCol[0])*255;
				}
				if (pixelCol[2] >=1){
					output1(x,y)->Blue = 255;
				} else {
					output1(x, y)->Blue = std::abs(pixelCol[2])*255;
				}
				if (pixelCol[1] >=1){
					output1(x, y)->Green = 255;
				} else {
					output1(x, y)->Green = std::abs(pixelCol[1])*255;
				}
			}
		}
		output1.WriteToFile(output_bmp_cube);
		std::cout << "*********************************************" << std::endl;
		std::cout << std::endl;
		std::cout << "*********************************************" << std::endl;
		std::cout << std::endl;
	}
	std::cout << std::endl; 

	return 0;
}

